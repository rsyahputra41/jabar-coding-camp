var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const baca = (time, books, index) => {
    if(index < books.length){
        readBooksPromise(time, books[index], function(time){
            if (time > 0){
                index += 1;
                baca (time, books, index)
            }
        });
    }
}
baca(10000, books, 0)