// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
  //readBooks(10000, books [0], readBooks)
  const baca = (sisa, books, index) =>{
      readBooks(sisa, books[index], function(time) {
          const lanjut = index + 1;
          if (lanjut < books.length){
              baca(time, books, lanjut);
          }
      });
  }

baca(10000, books, 0);
