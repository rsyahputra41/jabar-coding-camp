// soal 1

//buatlah variabel-variabel seperti di bawah ini
// var pertama = "saya sangat senang hari ini";
// var kedua = "belajar javascript itu keren";
// gabungkan variabel-variabel tersebut agar menghasilkan output

// saya senang belajar JAVASCRIPT

// jawaban soal 1
var batasatas = "==============================";
console.log(batasatas);

var jsoal1 = "jawaban soal 1";
console.log(jsoal1);

var batasatas1 = "==============================";
console.log(batasatas1);

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var kal1 = pertama.substring(0,4);
var kal2 = pertama.substring(11,19);
var kal3 = kedua.substring(0, 8);
var kal4 = kedua.substring(8, 18).toUpperCase();
console.log(kal1.concat(kal2.concat(kal3.concat(kal4))))

var batas1 = "==============================";
console.log(batas1);

// soal 2

// buatlah variabel-variabel seperti di bawah ini

// var kataPertama = "10";
// var kataKedua = "2";
// var kataKetiga = "4";
// var kataKeempat = "6";
// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika 
// semua variabel agar menghasilkan output 24 (integer).

// jawaban soal 2
var jsoal2 = "jawaban soal 2";
console.log(jsoal2);

var batasatas2 = "==============================";
console.log(batasatas2);

var kataPertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");
console.log(kataPertama+((kataKedua*kataKetiga)+kataKeempat));

var batas2 = "==============================";
console.log(batas2);

// soal 3

// buatlah variabel-variabel seperti di bawah ini

// var kalimat = 'wah javascript itu keren sekali'; 

// var kataPertama = kalimat.substring(0, 3); 
// var kataKedua; // do your own! 
// var kataKetiga; // do your own! 
// var kataKeempat; // do your own! 
// var kataKelima; // do your own! 

// console.log('Kata Pertama: ' + kataPertama); 
// console.log('Kata Kedua: ' + kataKedua); 
// console.log('Kata Ketiga: ' + kataKetiga); 
// console.log('Kata Keempat: ' + kataKeempat); 
// console.log('Kata Kelima: ' + kataKelima);
// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

// jawaban soal 3
var jsoal3 = "jawaban soal 3";
console.log(jsoal3);

var batasatas3 = "==============================";
console.log(batasatas3);

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(14, 18);
var kataKeempat= kalimat.substring(18, 24);
var kataKelima= kalimat.substring(24, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

var batas3 = "==============================";
console.log(batas3);