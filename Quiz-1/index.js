// 2.
// Function Penghitung Jumlah Kata

// Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

// Contoh
// var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
// var kalimat_2 = " Saya Iqbal"
// var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

// jumlah_kata(kalimat_1) // 6
// jumlah_kata(kalimat_2) // 2
// jumlah_kata(kalimat_3) // 4

// *catatan
// Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat

// Jawaban Soal 2

function jumlah_kata(str) { 
    return str.trim().split(" ").length;
}
    var kalimat_1 = jumlah_kata(" Halo nama saya Muhammad Iqbal Mubarok ");
    var kalimat_2 = jumlah_kata(" Saya Iqbal")
    var kalimat_3 = jumlah_kata(" Saya Muhammad Iqbal Mubarok ");
  
console.log(kalimat_1, kalimat_2, kalimat_3);
