//Soal 1
//buatlah variabel seperti di bawah ini

//var nilai;
 
//pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

//nilai >= 85 indeksnya A
//nilai >= 75 dan nilai < 85 indeksnya B
//nilai >= 65 dan nilai < 75 indeksnya c
//nilai >= 55 dan nilai < 65 indeksnya D
//nilai < 55 indeksnya E

//Jawaban Soal 1
var nilai = 55;

if (nilai >= 85){
    console.log("A");
    }else if (nilai >= 75 && nilai <85){
        console.log("B");
    }else if (nilai >= 65 && nilai < 75){
        console.log("C");
    }else if (nilai >= 55 && nilai < 65){
        console.log("D");    
    }else{
    console.log("E");
} 

//Soal 2
//buatlah variabel seperti di bawah ini

//var tanggal = 22;
//var bulan = 7;
//var tahun = 2020;
//ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

//Jawaban Soal 2
var tanggal = 22;
var bulan = 2;
var tahun = 1997;
switch(bulan){
  case 1: {console.log(tanggal + " Januari " + tahun);break;}
  case 2: {console.log(tanggal + " Februari " +tahun);break;}
  case 3: {console.log(tanggal + " Maret " + tahun);break;}
  case 4: {console.log(tanggal + " April " + tahun );break;}
  case 5: {console.log(tanggal + " Mei " + tahun);break;}
  case 6: {console.log(tanggal + " Juni " + tahun);break;}
  case 7: {console.log(tanggal + " Juli " + tahun);break;}
  case 8: {console.log(tanggal + " Agustus " + tahun);break;}
  case 9: {console.log(tanggal + " September " + tahun);break;}
  case 10: {console.log(tanggal + " Oktober " + tahun);break;}
  case 11: {console.log(tanggal + " November " + tahun);break;}
  case 12: {console.log(tanggal + " Desember " + tahun);break;}
  default: {console.log("Error");}
}

//Soal 3
//Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

//Output untuk n=3 :

//#
//##
//###

//Output untuk n=7 :
//#
//##
//###
//####
//#####
//######
//#######

//Jawaban Soal 3
var n = '';
for (let i = 0; i < 3; i++){
    for (let j = 0; j <= i; j++){
      n += '#';
    }
    n += '\n';
}
  console.log(n);

var a = '';
for (let b = 0; b < 7; b++){
    for (let c = 0; c <= b; c++){
      a += '#';
    }
    a += '\n';
}
  console.log(a);

//Soal 4

//berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
//contoh :

//Output untuk m = 3

//1 - I love programming
//2 - I love Javascript
//3 - I love VueJS
//===

//Output untuk m = 5

//1 - I love programming
//2 - I love Javascript
//3 - I love VueJS
//===
//4 - I love programming
//5 - I love Javascript

//Output untuk m = 7

//1 - I love programming
//2 - I love Javascript
//3 - I love VueJS
//===
//4 - I love programming
//5 - I love Javascript
//6 - I love VueJS
//======
//7 - I love programming

//Output untuk m = 10

//1 - I love programming
//2 - I love Javascript
//3 - I love VueJS
//===
//4 - I love programming
//5 - I love Javascript
//6 - I love VueJS
//======
//7 - I love programming
//8 - I love Javascript
//9 - I love VueJS
//=========
//10 - I love programming

//Jawaban Soal 4

var m = 10;
var n = '';
var pr = "programming";
var js = "javascript";
var va = "vueJs";

for (let i = 1; i <= m; i++){
      if (i % 3 === 1 ){
      console.log(i + " - I Love " + pr);
      }else if (i % 3 === 2 ){
       console.log(i + " - I Love " + js);
      }else if (i % 3 === 0 ){
       console.log(i + " - I Love " + va);
       for (let o = 0; o < 1; o++){
            for (let p = 0; p <= o; p++){
             n += '===';
            }
          }
       if (i % 3 === 0){
       console.log(n);
     }
     } 
}  