// Soal 1

// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

// Jawaban No 1
console.log("Jawaban No 1");
const luasPP = (p, l) => p * l;
console.log("Luas Persegi Panjang =" + " "+luasPP(2,3));

const kelPP = (p, l) => 2 * (p + l);
console.log("Keliling Persegi Panjang =" + " "+kelPP(2,2));

// Soal 2
 
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
      
//     const newFunction = function literal(firstName, lastName){
//       return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function(){
//           console.log(firstName + " " + lastName)
//         }
//       }
//     }
     
//     //Driver Code 
//     newFunction("William", "Imoh").fullName() 

// Jawaban No 2
console.log("Jawaban No 2");
const newFunction = (firstName, lastName) => {
    return {
      fullName(){
        console.log(firstName + " " + lastName)
        }
      }
    }
     
    //Driver Code 
    newFunction("William", "Imoh").fullName() 

// Soal 3
// Diberikan sebuah objek sebagai berikut:
    
// const newObject = {
//   firstName: "Muhammad",
//   lastName: "Iqbal Mubarok",
//   address: "Jalan Ranamanyar",
//   hobby: "playing football",
// }
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
    
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
    
// // Driver code
// console.log(firstName, lastName, address, hobby)

// Jawaban No 3
console.log("Jawaban No 3");
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  
const {firstName, lastName, address, hobby} = newObject
  
// Driver code
console.log(firstName, lastName, address, hobby)

// soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

// Jawaban No 4
console.log("Jawaban No 4");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code]
console.log(combined);

// soal 5

// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

// Jawaban No 5
console.log("Jawaban No 5");
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);