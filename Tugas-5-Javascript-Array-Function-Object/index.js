// Soal No
// buatlah variabel seperti di bawah ini

//  var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// Jawaban No 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (let i = 0; i < daftarHewan.length; i++){
  console.log(daftarHewan[i]);
}

// Soal No 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]" !

// /* 
//     Tulis kode function di sini
// */
 
// var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
// var perkenalan = introduce(data)
// console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// Jawaban No 2
function introduce(){
  return ("Nama saya " + data.name + " umur saya " + data.age + " tahun " + " alamat saya " + 
         data.address + " hobi saya " + data.hobby);
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal No 3

// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

// var hitung_1 = hitung_huruf_vokal("Muhammad")

// var hitung_2 = hitung_huruf_vokal("Iqbal")

// console.log(hitung_1 , hitung_2) // 3 2

// Jawaban No 3
function hitung_huruf_vokal(str) { 
  let hitung = 0; 
  var vokal = "aeiou"; 

  for (let huruf of str.toLowerCase()) { 
      if (vokal.includes(huruf)) { 
          hitung++; 
      } 
  } 
  return hitung 
} 

var hitung_1 = hitung_huruf_vokal("Raka"); 
var hitung_2 = hitung_huruf_vokal("Nursyahputra Pratama"); 

console.log(hitung_1, hitung_2);

// Soal No 4
// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

// console.log( hitung(0) ) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8

// Jawaban No 4
var kurang = 2;
var hasil = 0;

function hitung(n){
    hasil = n-kurang;
    kurang--;
}
for (var m = 0; m<6; m++){
  hitung(m);
  if (m!=4)
  console.log(hasil);
}